// dictionary to keep track of frame count for each animation
let frameCounts = {};

function animate(id, delay) {

  // get the container and frames for the amination
  const container = document.getElementById(id);
  const frames = container.children;

  // set up the frame counter
  frameCounts[id] = 0;

  // hide all frames except for the first
  frames[0].style.display = "flex";
  for (let i = 1; i < frames.length; i++) {
    frames[i].style.display = "none";
  }

  // start the animation
  const interval = setInterval(updateAnimation, delay, id, frames, frames.length);

}

function updateAnimation(id, frames, totalFrames) {

  // increment the frame counter for the given id
  frameCounts[id] = (frameCounts[id] + 1) % totalFrames;

  // show the next frame
  frames[frameCounts[id]].style.display = "flex";

  // hide the previous frame
  if (frameCounts[id] == 0) {
    frames[totalFrames - 1].style.display = "none";
  } else {
    frames[frameCounts[id] - 1].style.display = "none";
  }

}

// If the user have a setting on their device to minimize the amount of non-essential motion
const preferReduceMotion = window.matchMedia("(prefers-reduced-motion)").matches;

animate("illustration-accueil", preferReduceMotion ? 1500 : 500); // Reduce framerate if use preference is to reduce motion
animate("rennes", preferReduceMotion ? 3000 : 1000); // Reduce framerate if use preference is to reduce motion
animate("orsay", preferReduceMotion ? 4000 : 2000); // Reduce framerate if use preference is to reduce motion
animate("parterre", 1500);
