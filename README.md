# deuxfleurs.fr - site web

Le site web en ASCII Art de Deuxfleurs

```
                                         '\ ; /'    
                                        ·`.;.;.`·   
                                       ·— ·   · —·  
                                        ·`';.;'`·   
                                         '/ ; \'    
             .-.                                    
           .( ; ;                                   
     .-.  (_.():')                                  
   _( ' ; /(' ; .)                                  
  ( `(·)` ; ``-'                                    
  ',_' `·,'                                         
     `-`\|            +---------------------+       
         . /)         |  B i e n v e n u e  |       
       (\|/           +---------------------+       
         |              |                 |         
.,.,\/,}\|//,,, ;/,,\/  |  , \,,,;/. ;.;, |,,,,;,;..
                                                    

Fabriquons un internet convivial ⤵
```

## Structure

 - `static` - Ce qui doit être envoyé sur le bucket
   - `r` - Pour faire un système de redirection minimaliste. Exemple: `deuxfleurs.fr/r/rdv23`
   - `.well-known` - Utilisé par Matrix, Thunderbird, chatons.org et d'autres pour la configuration automatique de pas mal de services
   - `img` le dossier qui contient les images
 - `res` - Ce qui peut être utile pour modifier le site web
   - `calendar.txt` - Tous les calendriers ASCII jusqu'à Décembre 2024

## Générateur de site statique ?

Pas maintenant.

## Déployer en pré-production

Fait automatiquement via Woodpecker sur la branche `preprod`.

Le site en pré-production est ensuite accessible sur <https://preprod-site.web.deuxfleurs.fr/>

## Déployer en production

Fait automatiquement via Woodpecker sur la branche `main`.

## Déployer en production à la main (pas recommandé)

En cas de déploiement manuel : ⚠️ NE FAITES PAS `--delete` car il est fort probable
que des trucs qui doivent rester dans le bucket ne sont pas copiés ici.

```
aws s3 sync static/ s3://deuxfleurs.fr
```
