# DeuxFleurs, un site accessible ? -- Sommaire exécutif

_Lundi 12 février 2024, par Adrian Rosin & Adrien Luxey-Bitri_

## Des principes solides

Dans l’ensemble, deuxfleurs a posé des bases solides en termes d’accessibilité. Le code est correctement structuré et pensé pour garantir une accessibilité maximale. Il met en pratique les principaux standards d’accessibilité issus de W3C, ce qui témoigne de la qualité du travail réalisé. Il faut également noter que pour chaque élément du site, des recherches ont été réalisées, rien n'a été laissé au hasard et une attention méticuleuse quant à l'accessibilité a été pensée.

Par conséquent, on s’aperçoit que deuxfleurs, avant même de passer des tests fonctionnels, valide la plupart des principes d’accessibilité sur le web. Le site valide par exemple les instructions que met à disposition Access42 (https://access42.net/ressources/site-web-accessible/), une coopérative française qui réalise des audits sur l’accessibilité numérique et dont les critères post-audit permettent dans un premier temps de savoir si un site est sur la bonne direction ou non.

## Tests fonctionnels 

Si l’on passe le site sur des outils permettant de vérifier l’accessibilité (ADA, WCAG, WCAG2, … Pour plus d'informations, voir https://www.w3.org/WAI/ER/tools//index.html), celui-ci passe plus des trois quarts des tests concernant l’accessibilité. Les tests critiques passant dans l’intégralité. Les seuls tests non validés sont des erreurs potentielles sujettes à interprétation, qui résultent d'un choix conscient chez Deuxfleurs (commande artistique). 

Pour donner des exemples de suggestions d'améliorations d'accéssibilité que l'on peut proposer après une première analyse du site :
l’utilisation de balises `svg` à la place de balises préformatées `pre` pour gérer l’ASCII art en terme d’image et non en terme de texte ;
l'affichage des calendriers en tant qu’objet (`table`) plutôt qu’en texte sous balise préformatée ;
l'augmentation du contraste texte/fond…

En deuxième analyse, on comprend que les idées ci-dessus ont été analysées, et que les solutions retenues résultent d'un compromis entre accessibilité et démarche artistique.

## Commande artistique

Il a été demandé à la créatrice du site de concevoir un site simple et léger (sans image sauf l'illustration de Ronce). 
De plus, le code HTML se lit comme un journal, où ASCII art et textes se succèdent sans friction. 
On comprend mieux alors, que l'usage de `svg` contredit l'esprit recherché, puisqu'il ajoute une lourdeur inutile au code source.

## En pratique

Le site dans son ensemble actuel peut être considéré comme accessible. Lorsque le site est utilisé avec un narrateur, tout le contenu est décrit de manière simple et fluide assurant une bonne compréhension de celui-ci pour tous les utilisateurs. Malgré cela, le résultat obtenu diffère en fonction du système d’exploitation, du narrateur utilisé ou même du paramétrage de celui-ci. 

A ce titre, une expérience à plus grande échelle impliquant des personnes directement concernées par les lecteurs d’écran pourrait nous en apprendre davantage quant à l’accessibilité de deuxfleurs.

## Nous contacter

Si vous souhaitez nous faire des retours concernant l’accessibilité du site deuxfleurs, vous pouvez nous contacter [ici](https://matrix.to/#/#accessibilite:deuxfleurs.fr) ou via coucou _chez_ deuxfleurs.fr 